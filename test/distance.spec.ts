import { Coordinates } from '../src/coordinates';
import { Distance } from '../src/distance';

describe('Distance', () => {
    const londonOffice = new Coordinates('51.526', '-0.0827');
    const sanFranciscoOffice = new Coordinates('37.789', '-122.4');

    /**
     * I used https://www.cactus2000.de/uk/unit/massgrk.shtml to sanity check my results
     * as well as common sense, keeping in mind that my calculations are for a sphere, not an ellipsoid
     */
    describe('when only start coordinates are provided', () => {
        it('should return 464.45 KM distance between the London and Dublin office', () => {
            const distance = Distance.distanceBetweenCoordinates(londonOffice);
            expect(distance).toEqual(464.45);
        });
    });

    describe('when start and end coordinates are provided', () => {
        it('should return 8614.82 KM distance between the London and San Francisco office', () => {
            const distance = Distance.distanceBetweenCoordinates(londonOffice, sanFranciscoOffice);
            expect(distance).toEqual(8614.82);
        });
    });
});
