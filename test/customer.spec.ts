import { RawCustomerData } from '../src/models/raw-customer-data.model';
import { Customer } from '../src/customer';

describe('Customer', () => {
    let rawCustomerData: RawCustomerData;

    beforeEach(() => {
        rawCustomerData = {
            user_id: 1,
            name: 'John Snow',
            latitude: '45',
            longitude: '45',
        };
    });

    it('should assign user_id, name and coordinates when creating a customer', () => {
        const customer = new Customer(rawCustomerData);

        expect(customer.user_id).toEqual(1);
        expect(customer.name).toEqual('John Snow');
        expect(customer.coordinates).toEqual({ latitude: 45, longitude: 45 });
    });

    it('should throw when user_id is null', () => {
        rawCustomerData.user_id = null as any;

        expect(() => new Customer(rawCustomerData)).toThrow('Invalid user ID');
    });

    it('should throw when user_id is not a number', () => {
        rawCustomerData.user_id = 'four' as any;

        expect(() => new Customer(rawCustomerData)).toThrow('Invalid user ID');
    });

    it('should throw when name is null', () => {
        rawCustomerData.name = null as any;

        expect(() => new Customer(rawCustomerData)).toThrow('Invalid user name');
    });

    it('should throw when name is not a string', () => {
        rawCustomerData.name = 42 as any;

        expect(() => new Customer(rawCustomerData)).toThrow('Invalid user name');
    });

    describe('getCustomers', () => {
        afterEach(() => {
            jest.restoreAllMocks();
        });

        it('should log the error and exit when there is a problem with the file', () => {
            const logSpy = jest.spyOn(console, 'error').mockImplementation(() => {});
            const processSpy = jest.spyOn(process, 'exit').mockImplementation(() => {
                return undefined as never;
            });

            Customer.getCustomers('non-existent.txt');

            expect(logSpy).toHaveBeenCalledWith(expect.stringContaining('Error reading data file: ENOENT: no such file or directory'));
            expect(processSpy).toHaveBeenCalledWith(1);
        });

        it('should create and return customers from the file', () => {
            const customers = Customer.getCustomers('../test/test-data/all-valid-customers.txt');

            expect(customers.errors).toHaveLength(0);
            expect(customers.data).toHaveLength(2);
            expect(customers.data[0]).toBeInstanceOf(Customer);
            expect(customers.data[1]).toBeInstanceOf(Customer);
        });

        it('should return all valid customers and errors for invalid data', () => {
            const customers = Customer.getCustomers('../test/test-data/some-valid-customers.txt');

            expect(customers.data).toHaveLength(2);
            expect(customers.errors).toHaveLength(2);
            expect(customers.errors[0]).toMatch('Error parsing customer data');
            expect(customers.errors[1]).toMatch('Error parsing customer data');
        });
    });
});
