import { Coordinates } from '../src/coordinates';

describe('Coordinates', () => {
    it('should assign latitude and longitude when given valid coordinate values', () => {
        const coordinates = new Coordinates('53.339428', '-6.257664');
        expect(coordinates).toEqual({ latitude: 53.339428, longitude: -6.257664 });
    });

    it('should throw when given latitude that is not a number', () => {
        expect(() => new Coordinates('forty two', '-6.257664')).toThrow('Invalid latitude: forty two');
    });

    it('should throw when given invalid latitude', () => {
        expect(() => new Coordinates('91', '-6.257664')).toThrow('Invalid latitude: 91');
    });

    it('should throw when given longitude that is not a number', () => {
        expect(() => new Coordinates('53.339428', 'not a number')).toThrow('Invalid longitude: not a number');
    });

    it('should throw when given invalid longitude', () => {
        expect(() => new Coordinates('53.339428', '181')).toThrow('Invalid longitude: 181');
    });
});
