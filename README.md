# Intercom Interview

## Prerequisites

-   [NodeJS](https://nodejs.org/en/download/)

## Running the program

**Install dependencies**

```bash
npm install
```

**Build and run**

```bash
npm run build && npm start
```

## Tests

Jest is used as a testing framework. There are unit tests covering most of the functionality. To run tests:

```bash
npm run test
```

## Notes

First of, thanks for taking the time to review my code 👀

The solution uses Typescript to enforce static typing and object oriented programming to separate concerns and allow for code reusability.

I've tried to strike the right balance between too little and too much code separation. For example, the method `getCustomers` is a static method inside the `Customer` class. It could also live in a separate `CustomerService`, but it doesn't seem necessary given the scope of the problem.

Time is limited, so the main focus was on solving the problem and having production-ready code, including tests. Given more time, I would have liked to add optional command line arguments for:

-   providing a path to any data file
-   providing different coordinates
-   setting a custom radius
-   providing an output path for the result

Looking forward to any feedback. Thank you for your consideration 🤘
