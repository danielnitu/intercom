import { Coordinates } from './coordinates';

/**
 * Mean radius in KM
 * https://en.wikipedia.org/wiki/Earth_radius
 */
const EARTH_RADIUS: number = 6371;

const DUBLIN_COORDINATES: Coordinates = new Coordinates('53.339428', '-6.257664');

export class Distance {
    /**
     * Return distance in KM, rounded to two decimals, between 2 coordinates.
     *
     * The formula used is for spheres, not WGS84 ellipsoids
     * so there is a small difference to the real world distance.
     *
     * https://en.wikipedia.org/wiki/Great-circle_distance
     * acos ( sinLat1 * sinLat2 + cosLat1 * cosLat2 * cos(long2 - long1) )
     */
    public static distanceBetweenCoordinates(start: Coordinates, end: Coordinates = DUBLIN_COORDINATES): number {
        const [startLat, startLong, endLat, endLong] = [
            Distance.degreesToRadians(start.latitude),
            Distance.degreesToRadians(start.longitude),
            Distance.degreesToRadians(end.latitude),
            Distance.degreesToRadians(end.longitude),
        ];

        const centralAngle = Math.acos(
            Math.sin(startLat) * Math.sin(endLat) + Math.cos(startLat) * Math.cos(endLat) * Math.cos(startLong - endLong),
        );

        const distance = EARTH_RADIUS * centralAngle;

        return Math.round((distance + Number.EPSILON) * 100) / 100;
    }

    private static degreesToRadians(degrees: number): number {
        return degrees * (Math.PI / 180);
    }
}
