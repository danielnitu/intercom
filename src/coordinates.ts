export class Coordinates {
    public latitude: number;
    public longitude: number;

    constructor(latitude: string, longitude: string) {
        this.latitude = this.parseLatitude(latitude);
        this.longitude = this.parseLongitude(longitude);
    }

    private parseLatitude(latitude: string): number {
        const lat = parseFloat(latitude);

        if (isFinite(lat) && Math.abs(lat) <= 90) {
            return lat;
        } else {
            throw new Error(`Invalid latitude: ${latitude}`);
        }
    }

    private parseLongitude(longitude: string): number {
        const lng = parseFloat(longitude);

        if (isFinite(lng) && Math.abs(lng) <= 180) {
            return lng;
        } else {
            throw new Error(`Invalid longitude: ${longitude}`);
        }
    }
}
