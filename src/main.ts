import { Customer } from './customer';
import { Distance } from './distance';

const INVITATION_RADIUS_IN_KM = 100;

const customers = Customer.getCustomers();
const result: Customer[] = [];

for (let customer of customers.data) {
    const distanceToCustomer = Distance.distanceBetweenCoordinates(customer.coordinates);

    if (distanceToCustomer <= INVITATION_RADIUS_IN_KM) {
        result.push(customer);
    }
}

result.sort((a, b) => a.user_id - b.user_id);

if (customers.errors.length) {
    console.log(`${customers.errors.length} errors encountered:`);
    customers.errors.forEach((error) => console.log(error));
    console.log('\n');
}

result.forEach((customer) => console.log(`${customer.name}, user_id: ${customer.user_id}`));
