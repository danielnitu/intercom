import * as path from 'path';
import * as fs from 'fs';
import { Coordinates } from './coordinates';
import { RawCustomerData } from './models/raw-customer-data.model';
import { Payload } from './models/payload.model';

const DATA_PATH = '../data/customers.txt';

export class Customer {
    public user_id: number;
    public name: string;
    public coordinates: Coordinates;

    constructor(rawData: RawCustomerData) {
        this.user_id = this.parseId(rawData.user_id);
        this.name = this.parseName(rawData.name);
        this.coordinates = new Coordinates(rawData.latitude, rawData.longitude);
    }

    // This method could live in its own class/module, but it doesn't seem necessary at this stage
    public static getCustomers(filePath: string = DATA_PATH): Payload<Customer[]> {
        let rawDataLines: string[];

        const result: Payload<Customer[]> = {
            data: [],
            errors: [],
        };

        try {
            // Read file, split on newline of both UNIX and Windows systems and ignore empty lines
            rawDataLines = fs.readFileSync(path.resolve(__dirname, filePath), 'utf8').split(/[\r\n]+/);
        } catch (error) {
            console.error('Error reading data file: ' + error.message);
            return process.exit(1);
        }

        for (const line of rawDataLines) {
            try {
                const customer = new Customer(JSON.parse(line));
                result.data.push(customer);
            } catch (error) {
                result.errors.push(`Error parsing customer data: ${error.message} LINE: ${line || null} `);
            }
        }

        return result;
    }

    private parseId(id: any): number {
        if (!id || isNaN(id)) {
            throw new Error('Invalid user ID');
        } else {
            return parseInt(id);
        }
    }

    private parseName(name: any): string {
        if (!name || typeof name !== 'string') {
            throw new Error('Invalid user name');
        } else {
            return name;
        }
    }
}
