export interface RawCustomerData {
    user_id: number;
    name: string;
    latitude: string;
    longitude: string;
}
