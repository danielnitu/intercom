export interface Payload<T> {
    data: T;
    errors: string[];
}
